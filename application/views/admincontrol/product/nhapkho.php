<?php 
$db =& get_instance();
$userdetails=$db->userdetails();
?>
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/plugins/ui/jquery-ui.min.css") ?>">
<script type="text/javascript" src="<?= base_url('assets/plugins/ui/jquery-ui.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/plugins/select2/select2.min.css") ?>">
<script type="text/javascript" src="<?= base_url('assets/plugins/select2/select2.full.min.js') ?>"></script>
<style>
	.jscolor-picker-wrap{
		z-index:999999 !important;
	}

	#product-variations.table > tbody > tr > td:first-child {
		max-width:50px;
	}

	#product-variations.table > tbody > tr > td:last-child {
		max-width:50px;
		text-align:right;
	}

	#product-variations.table > tbody > tr > td, #product-variations.table > tfoot > tr > td, #product-variations.table > thead > tr > td {
		padding: 5px 12px;
		vertical-align: middle;
	}
	.btn_active {
		background-color: #8f9499 !important;
		border-color: #8f9499 !important;
	}
</style>

<?php if($this->session->flashdata('success')){?>
	<div class="alert alert-success alert-dismissable my_alert_css">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('success'); ?> </div>
	<?php } ?>
	<?php if($this->session->flashdata('error')){?>
		<div class="alert alert-danger alert-dismissable my_alert_css">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $this->session->flashdata('error'); ?> </div>
		<?php } ?>

<form class="form-horizontal" method="post" action=""  enctype="multipart/form-data" id="form_form">
<div class="row">
<div class="col-sm-12">
<div class="card">
<div class="card-header bg-blue-payment">
	<div class="card-title-white pull-left m-0">Nhập hàng</div>
</div>
<div class="card-body">	
	<input type="hidden" id="product_id" name="product_id" value="<?php echo $product->product_id ?>">
	<div class="form-group">
		<label class="col-form-label"><?= __('admin.product_name') ?></label>
		<div>
			<input placeholder="<?= __('admin.enter_your_product_name') ?>" name="product_name" value="<?php echo $product->product_name; ?>" class="form-control" type="text" readonly>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">			
			<div class="form-group">
				<label class="col-form-label"><?= __('admin.product_sku') ?> </label>
				<div>
					<input placeholder="<?= __('admin.enter_your_product_sku') ?>" name="product_sku" id="product_sku" class="form-control" value="<?php echo $product->product_sku; ?>" type="text" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-form-label">Tồn kho </label>
				<div>
					<input placeholder="Nhập số lượng tồn kho" name="ton_kho" id="ton_kho" class="form-control" value="<?php echo $product->ton_kho; ?>" type="number" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-form-label">Số lượng nhập </label>
				<div>
					<input placeholder="Nhập số lượng tồn kho" name="sl_nhap" id="sl_nhap" class="form-control" value="" type="number" >
				</div>
			</div>
			
		</div>
		
	</div>


			<div class="mt-4">
				<div class="text-right">
					<button type="submit" class="btn btn-lg btn-default btn-submit btn-success" name="save"><?= __('admin.save') ?></button>
				</div>
			</div>
		</div>
	</div>
</form>


<script type="text/javascript">
	$(".btn-submit").on('click',function(evt){
		evt.preventDefault();
		$btn = $(this);
		var formData = new FormData($("#form_form")[0]);

		formData.append("action", $(this).attr("name"));

		formData = formDataFilter(formData);
		$this = $("#form_form");	       

		$btn.btn("loading");
		$.ajax({
			url:'<?= base_url('admincontrol/nhapkhoSP') ?>',
			type:'POST',
			dataType:'json',
			cache:false,
			contentType: false,
			processData: false,
			data:formData,
			xhr: function (){
				var jqXHR = null;

				if ( window.ActiveXObject ){
					jqXHR = new window.ActiveXObject( "Microsoft.XMLHTTP" );
				}else {
					jqXHR = new window.XMLHttpRequest();
				}

				jqXHR.upload.addEventListener( "progress", function ( evt ){
					if ( evt.lengthComputable ){
						var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
						$('.loading-submit').text(percentComplete + "% "+'<?= __('admin.loading') ?>');
					}
				}, false );

				jqXHR.addEventListener( "progress", function ( evt ){
					if ( evt.lengthComputable ){
						var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
						$('.loading-submit').text("Save");
					}
				}, false );
				return jqXHR;
			},
			error:function(){ $btn.btn("reset"); },
			success:function(result){ 
				console.log(result);      	
				$btn.btn("reset");
				$('.loading-submit').hide();
				$this.find(".has-error").removeClass("has-error");
				$this.find("span.text-danger").remove();

				if(result['location']){
					window.location = result['location'];
				}
				if(result['errors']){
					$.each(result['errors'], function(i,j){
						$ele = $this.find('[name="'+ i +'"]');
						if($ele){
							$ele.parents(".form-group").addClass("has-error");
							$ele.after("<span class='text-danger'>"+ j +"</span>");
						}
					});
				}
			},
		});

		return false;
	});

$(document).on('change', '#recur_day, #recur_hour, #recur_minute', function(){
	var days = $('#recur_day').val();
	var hours = $('#recur_hour').val();
	var minutes = $('#recur_minute').val();
	var total_minutes;		

	total_hours = parseInt(days*24) + parseInt(hours);
	total_minutes = parseInt(total_hours*60) + parseInt(minutes);
	$('.custom_time').find('input[name="recursion_custom_time"]').val(total_minutes);

});
if($('#endtime').length) {

	$('#endtime').datetimepicker({
		format:'d-m-Y H:i',
		inline:true,
	});
}

$('#setCustomTime').on('change', function(){
	$(".custom_time_container").hide();
	if($(this).prop("checked")){
		$(".custom_time_container").show();
	}
});

$(document).on('ready',function() {
	$('input[name="product_type"]:checked').trigger('change');
	$('input[name="sub_product_type"]:checked').trigger('change');
	$('[name="allow_for"]').trigger("change");
	sumNote($('.summernote-img'));
});

var fileArray = [];

$('.downloadable_file_input').change(function(e){
	$.each(e.target.files, function(index, value){
		var fileReader = new FileReader(); 
		fileReader.readAsDataURL(value);
		fileReader.name = value.name;
		fileReader.rawData = value;
		fileArray.push(fileReader);
	});

	render_priview();
});

var getFileTypeCssClass = function(filetype) {
	var fileTypeCssClass;
	fileTypeCssClass = (function() {
		switch (true) {
			case /image/.test(filetype): return 'image';
			case /video/.test(filetype): return 'video';
			case /audio/.test(filetype): return 'audio';
			case /pdf/.test(filetype): return 'pdf';
			case /csv|excel/.test(filetype): return 'spreadsheet';
			case /powerpoint/.test(filetype): return 'powerpoint';
			case /msword|text/.test(filetype): return 'document';
			case /zip/.test(filetype): return 'zip';
			case /rar/.test(filetype): return 'rar';
			default: return 'default-filetype';
		}
	})();
	return fileTypeCssClass;
};

function render_priview() {
	var html = '';

	$.each(fileArray, function(i,j){
		html += '<tr>';
		html += '    <td width="70px"> <div class="upload-priview up-'+ getFileTypeCssClass(j.rawData.type) +'" ></div></td>';
		html += '    <td>'+ j.name +'</td>';
		html += '    <td width="70px"><button type="button" class="btn btn-danger btn-sm remove-priview" data-id="'+ i +'" ><?= __('admin.remove') ?></button></td>';
		html += '</tr>';
	})

	$("#priview-table tbody").html(html);
}


$("#priview-table").delegate('.remove-priview','click', function(){
	if(!confirm('<?= __('admin.are_you_sure') ?>')) return false;

	var index = $(this).attr("data-id");
	fileArray.splice(index,1);
	render_priview()
})
$("#priview-table-video").delegate('.remove-priview','click', function(){
	if(!confirm('<?= __('admin.are_you_sure') ?>')) return false;

	var index = $(this).attr("data-id");
	fileArrayVideo.splice(index,1);
	render_priview()
})

$(".remove-priview-server").on('click',function(){
	if(!confirm("<?= __('admin.are_you_sure') ?>")) return false;

	var attr = $(this).attr('data-main');

	if (typeof attr !== 'undefined' && attr !== false) {
		var index = $(this).attr("data-main");
		var name = $(this).attr("data-name");
		for (var i = 0; i < video_fileArr.length; i++) {
			if(video_fileArr[i][0] == index && video_fileArr[i][1].name ==name) {
				console.log("Value Matched and Deletedt at :",i);
				video_fileArr.splice(i,1);
			}
		}
	}
	$(this).parents("tr").remove();
})

			//Video File Uplaod
			var fileArrayVideo = [];
			$('.downloadable_file_input_video').change(function(e){
				$.each(e.target.files, function(index, value){
					var fileReader = new FileReader(); 
					fileReader.readAsDataURL(value);
					fileReader.name = value.name;
					fileReader.rawData = value;
					fileArrayVideo.push(fileReader);
				});

				render_priview_video();
			});

			function render_priview_video() {
				var html = '';

				$.each(fileArrayVideo, function(i,j){
					html += '<tr>';
					html += '    <td width="70px"> <div class="upload-priview up-'+ getFileTypeCssClass(j.rawData.type) +'" ></div></td>';
					html += '    <td>'+ j.name +'</td>';
					html += '    <td><input type="text" class="form-control" name="videotext[]" value="" placeholder="Add Video Title"></td>';
					html += '    <td width="70px"><button type="button" class="btn btn-danger btn-sm remove-priview" data-id="'+ i +'" ><?= __('admin.remove') ?></button></td>';
					html += '</tr>';
				})

				$("#priview-table-video tbody").html(html);
			}
			var fileArrayVideoText = [];
			function render_priview_video_link() {
				var html = '';

				$.each(fileArrayVideoText, function(i,j){
					html += '<tr>';
					html += '    <td width="70px"><input type="text" placeholder="Video Link"  name="videolink[]" class="form-control" ></td>';
					html += '    <td width="70px"><button type="button" class="btn btn-danger btn-sm remove-priview" data-id="'+ i +'" ><?= __('admin.remove') ?></button></td>';
					html += '</tr>';
				})

				$("#priview-table-video-link tbody").html(html);
			}

			$("#priview-table-video").delegate('.remove-priview','click', function(){
				if(!confirm('<?= __('admin.are_you_sure') ?>')) return false;

				var index = $(this).attr("data-id");
				fileArray.splice(index,1);
				render_priview_video()
			})

			$(document).on("click",".remove-local-uploaded",function(e){
				e.preventDefault();
				if(!confirm('<?= __('admin.are_you_sure') ?>')) return false;

				var index = $(this).attr("data-main");
				var name = $(this).attr("data-name");
				var zipname = $(this).attr("data-zip");
				for (var i = 0; i < video_fileArr.length; i++) {
					if(video_fileArr[i][0] == index && video_fileArr[i][1].name ==name) {
						video_fileArr.splice(i,1);
					}
				}
				for (var i = 0; i < video_fileZipArr.length; i++) {
					if(video_fileZipArr[i][0] == index && video_fileZipArr[i][1].name ==zipname) {
						video_fileZipArr.splice(i,1);
					}
				}
				$(this).parent().parent().remove();

			})

			$("#addMoreLinktext").click(function(event) {


				fileArrayVideoText.push(new Date());
				render_priview_video_link();
			});

			$('input[name="sub_product_type"]').on('change',function(){
				var val = $(this).val();
				$('.proSubType').removeClass('btn_active');
				$(this).parent().addClass('btn_active');
				if(val== 'video') {
					$('.video_file_uploader_div').show();
					$('.video_link_div').hide();

				} else {
					$('.video_file_uploader_div').hide();
					$('.video_link_div').show();

				}
			});
			$('input[name="product_type"]').on('change',function(){
				var val = $(this).val();
				if(val == 'downloadable'){ 
					$('.downloadable_file_div').show(); 
					$('.allow_shipping-option').hide(); 
					$('.video_file_div').hide();
					$('input[name="allow_shipping"]').parent().addClass('off');
					$('input[name="allow_shipping"]').val(0).change();
					$('input[name="allow_shipping"]').attr("disabled","disabled");
					$("#allow_shipping_div,#allow_upload_file_div").addClass('d-none');
				}
				else if(val=="video") {
					$('.video_file_div').show(); 
					$('.allow_shipping-option').hide(); 
					$('.downloadable_file_div').hide(); 
					$('input[name="allow_shipping"]').parent().addClass('off');
					$('input[name="allow_shipping"]').val(0).change();
					$('input[name="allow_shipping"]').attr("disabled","disabled");
					$("#allow_shipping_div,#allow_upload_file_div").addClass('d-none');
				}
				else{ 
					$('.video_file_div').hide(); 
					$('.downloadable_file_div').hide(); 
					$('.allow_shipping-option').show(); 
					$('input[ name="allow_shipping"]').removeAttr("disabled");
					$("#allow_shipping_div,#allow_upload_file_div").removeClass('d-none');
				}
			});

			$('.update_product_settings').on('change', function(){
				var checked = $(this).prop('checked');
				var setting_key = $(this).data('setting_key');
				var product_id = $(this).data('product_id');

				if (checked == true) {
					var status = 1;
				}else{
					var status = 0;
				}

				$.ajax({
					url:'<?= base_url("admincontrol/update_product_settings") ?>',
					type:'POST',
					dataType:'json',
					data:{'action':'update_all_settings', status:status, setting_key:setting_key, product_id:product_id},
					success:function(json){
					},
				})
			});
			var totalSection = $("#priview-table-video").find("fieldset").length;
			$("#add_section").on("click",function(e){
				e.preventDefault();
				var html =`<fieldset class="custom-design mb-2">
				<legend>Section <span>`+(totalSection+1)+`</span></legend>
				<div class="row mb-3">
				<div class="col-md-8">
				<input type="text" class="form-control" name="section[`+totalSection+`]" value="" placeholder="Title Section">
				</div>
				<div class="file-preview-button btn btn-small  btn-primary col-md-3">
				Video[Uploaded File]																			<input type="file" class="videoFileUploadIP" name="video_files[`+totalSection+`][]" data-value="`+totalSection+`" multiple="multiple">
				</div>
				<button class="btn btn-small btn-danger remove-section"><i class="fa fa-close"></i></button>
				</div>
				<table class="table table-hover videofile-preview" id="videofile-preview`+totalSection+`">
				<thead><tr>
				<th>Video File</th>
				<th>Title</th>
				<th>Description</th>
				<th>Action</th>
				</tr></thead>
				<tbody>
				</tbody>
				</table>
				</fieldset>`;
				$("#priview-table-video").append(html); 
				totalSection++; 
			});
			$(document).on("click",".remove-section",function(){
				if(!confirm('<?= __('admin.are_you_sure') ?>')) return false;

				
				totalSection--;
				totalSectionlink--;
				var localVideoElements = $(this).parent().parent().find('table').find('.remove-local-uploaded,.remove-priview-server');
				localVideoElements.each(function(index, el) {
					var attr = $(this).attr('data-main');
					if (typeof attr !== 'undefined' && attr !== false) {

						var index = $(this).attr("data-main");
						var name = $(this).attr("data-name");
						var zipname = $(this).attr("data-zip");
						for (var i = 0; i < video_fileArr.length; i++) {
							if(video_fileArr[i][0] == index && video_fileArr[i][1].name ==name) {
								video_fileArr.splice(i,1);
							}
						}

						for (var i = 0; i < video_fileZipArr.length; i++) {
							if(video_fileZipArr[i][0] == index && video_fileZipArr[i][1].name ==zipname) {
								video_fileZipArr.splice(i,1);
							}
						}
					}
				});
				$(this).parent().parent().remove();
				console.log(video_fileArr);
			});
			function getFileSize(_size) {
				var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
				i=0;while(_size>900){_size/=1024;i++;}
				var exactSize = (Math.round(_size*100)/100)+' '+fSExt[i];
				return exactSize;
			}


			var uploadedVideosDurations = 0;

			window.URL = window.URL || window.webkitURL;

			async function setFileInfo(that, _callback) {
			  var files = that.files;
			  uploadedVideosDurations = 0
			  var video = document.createElement('video');
			  video.preload = 'metadata';

			  video.onloadedmetadata = await function() {
			    window.URL.revokeObjectURL(video.src);
			    var duration = video.duration;
			    uploadedVideosDurations = duration;

			    _callback();
			  }

			  video.src = URL.createObjectURL(files[0]);;
			}


			$(document).on("change",".videoFileUploadIP", async function(e){
				
				that = $(this);

				await setFileInfo(this, function() {
					var id =$(that).data('value');
					
					var newRow ="";
					for (var i = 0; i < e.target.files.length; i++) {
						var rsID = Math.floor((Math.random() * 100000000) + 1);
						e.target.files[i];

						let updatedFile = e.target.files[i];
						updatedFile.duration = uploadedVideosDurations

						video_fileArr.push([id, updatedFile]);
						var  fileSize = getFileSize(e.target.files[i].size);
						newRow +=`<tr>
						<td>`+e.target.files[i].name+`( <strong>`+fileSize+`</strong> )`+`
						<div class="mt-3">
						<input type="checkbox" name="iszipResource[`+id+`][]" value="0" class="isResource" id="`+rsID+`">
						<label for="resource`+rsID+`" class="ml-1 form-check-label mb-3">
						Lesson Resource
						</label>
						</div>
						<div class="resource d-none" id="resource`+rsID+`">
						<p></p><input type="file" data-main="`+id+`" class="VideoFileResource" name="VideoFileZip[`+id+`][]">
						<p></p>
						<input type="text" name="VideoFileResourceText[`+id+`][]" value="" placeholder="Resource Name" class="form-control mt-3">
						</div>
						</td>
						<td><input type="text" class="form-control" name="videotext[`+id+`][]" value="" placeholder="Add Video Title"></td>
						<td><input type="text" class="form-control" name="description[`+id+`][]" value="" placeholder="Add Video Description"></td>
						<td width="70px"><button type="button" class="btn btn-danger btn-sm remove-local-uploaded" data-name="`+e.target.files[i].name+`" data-main="`+id+`"><?= __('admin.remove') ?></button></td></tr>`;
					}


					console.log(video_fileArr);
					$("#priview-table-video").find("#videofile-preview"+id+" tbody").append(newRow);
				});
			});

			$(document).on("click",".remove-priview-video",function(){
				if(!confirm('<?= __('admin.are_you_sure') ?>')) return false;

				$(this).parent().parent().remove();
			});
			var totalSectionlink = $("#priview-table-video-link").find("fieldset").length;
			$(document).on("click","#add_section_link",function(e){
				e.preventDefault();
				var videoLink ='<?=__('admin.video_product_link')?>';
				var html =`<fieldset class="custom-design mb-2">
				<legend>Section <span>`+(totalSectionlink+1)+`</span></legend>
				<div class="row mb-3">
				<div class="col-md-8">
				<input type="text" class="form-control" name="sectionlink[`+totalSectionlink+`]" value="" placeholder="Title Section">
				</div>
				<div class="file-preview-button btn btn-small  btn-primary col-md-3">`+videoLink+`<input type="button" class="addNewText" data-value="`+totalSectionlink+`" >
				</div>
				<button class="btn btn-small btn-danger remove-section"><i class="fa fa-close"></i></button>
				</div>
				<table class="table table-hover videolink-preview" id="videolink-preview`+totalSectionlink+`">
				<thead><tr>
				<th>Video File</th>
				<th>Title</th>
				<th>Description</th>
				<th>Action</th>
				</tr></thead>
				<tbody>
				</tbody>
				</table>
				</fieldset>`;
				$("#priview-table-video-link").append(html); 
				totalSectionlink++;
			});

			$(document).on("click",".addNewText",function(e){
				e.preventDefault();
				var id =$(this).data('value');
				var rsID = Math.floor((Math.random() * 100000000) + 1);
				
				var currentEl =$("input[name='VideoFileZip["+id+"][]']").length;
				
				var newRow =`<tr>
				<td>
				<input type="text" class="form-control" name="videolink[`+id+`][]" placeholder="Enter Video Link">
				<div class="mt-3">
				<input type="checkbox" name="iszipResource[`+id+`][]" value="0" class="isResource" id="`+rsID+`">
				<label for="resource`+rsID+`" class="ml-1 form-check-label mb-3">
				Lesson Resource
				</label>
				</div>
				<div class="resource d-none" id="resource`+rsID+`">
				<p></p><input type="file" data-main="`+id+`" class="VideoFileResource" name="VideoFileZip[`+id+`][`+currentEl+`]" data-current="`+currentEl+`">
				<input type="text" name="VideoFileResourceText[`+id+`][]" value="" placeholder="Resource Name" class="form-control mt-3">
				</div>
				</td>
				<td><input type="text" class="form-control" name="videotext[`+id+`][]"  placeholder="Add Video Title"></td>
				<td><input type="text" class="form-control" name="description[`+id+`][]" value="" placeholder="Add Video Description"></td>
				<td width="70px"><button type="button" class="btn btn-danger btn-sm remove-priview-video">Remove</button></td>
				</tr>`;

				$("#priview-table-video-link").find("#videolink-preview"+id+" tbody").append(newRow);
			});

			$(".updateVideoFile").change(async function(e){

				let that = $(this);
				let that_e = e;
				await setFileInfo(this, function() {
					var id =$(that).data('main');
					var name =$(that).data('name');
					var oldname =$(that).data('old-name')
					e.target.files[0].duration = uploadedVideosDurations;
					video_fileArr.push([id,that_e.target.files[0],oldname]);

					$(that).parent().find("p").html(that_e.target.files[0].name + " (<strong>"+ getFileSize(that_e.target.files[0].size) +"</strong>)")
					$(that).parent().parent().find('button').attr('data-name',that_e.target.files[0].name)
					$(that).parent().parent().find('button').attr('data-main',id)
				});
				
			});
			$(".updateVideoFileResource").change(function(e){
				var ext = $(this).val().split('.').pop().toLowerCase();
				if('zip' != ext) {
					$(this).val('');
					alert('Only allow zip file');
					return false;
				}
				var id =$(this).data('main');
				var name =$(this).data('name');
				var oldname =$(this).data('old-name');
				video_fileZipArr.push([id,e.target.files[0],oldname]);

				$(this).parent().find("p").html(e.target.files[0].name + " (<strong>"+ getFileSize(e.target.files[0].size) +"</strong>)")
				$(this).parent().parent().find('button').attr('data-name',e.target.files[0].name)
				$(this).parent().parent().find('button').attr('data-main',id)
				
				console.log(video_fileZipArr);
			});

			$(document).on("change",".VideoFileResource",function(e){
				var ext = $(this).val().split('.').pop().toLowerCase();
				if('zip' != ext) {
					$(this).val('');
					alert('Only allow zip file');
					return false;
				}
				var id =$(this).data('main');
				if($("input[name='sub_product_type']:checked").val() == "videolink") {
					var current =$(this).data('current');
					video_fileZipArr.push([id,e.target.files[0],current]);
				} else {

					video_fileZipArr.push([id,e.target.files[0]]);
				}
				$(this).parent().find("p").html(e.target.files[0].name + " (<strong>"+ getFileSize(e.target.files[0].size) +"</strong>)")
				$(this).parent().parent().find('button').attr('data-zip',e.target.files[0].name);
				console.log(video_fileZipArr);
			});

			$(document).on('change','.isResource',function(){
				var id = $(this).attr('id');
				if($(this).is(':checked')) {
					$('#resource'+id).removeClass('d-none')
					$(this).val(1);
				} else {
					$(this).val(0);
					$('#resource'+id).addClass('d-none')
					$(document).find('#resource'+id).find('p').html('')
					$(document).find('#resource'+id).find('.updateVideoFileResource').val('')

				}
			});
			$(document).on('change','.updateResource',function(){
				var id = $(this).attr('id');
				if($(this).is(':checked')) {
					$('#resource'+id).removeClass('d-none')
					$(this).val(1);
				} else {
					if(confirm('Are you sure ?')) {
						$(this).val(0);
						
						$.ajax({
							url:'<?= base_url("admincontrol/lmsResourceupdate") ?>',
							type:'POST',
							dataType:'json',
							data:{ product_id:$("#product_id").val(),id:$(this).attr('id')},
							success:function(json){
							},
						})

						$('#resource'+id).addClass('d-none')
						$(document).find('#resource'+id).find('p').html('')
						$(document).find('#resource'+id).find('.updateVideoFileResource').val('')
					} else {
						$(this).val(1);
						$(this).click();
						return false;
					}


				}
			});
			$(document).on('click',".proType",function(e){
				e.preventDefault();
				var value =$(this).data('value');
				$(".proType").removeClass('btn_active');
				$(this).addClass('btn_active');
				$(`input[name="product_type"][value="`+value+`"]`).prop('checked',true).change();
				var title = value=='downloadable' ?'<?= __('admin.downloadable_product')?>': (value=='video' ? '<?= __('admin.lms_product')?>':'');
				if(title=='')
					$("#product_type_title").parent().addClass('d-none')
				else		
					$("#product_type_title").parent().removeClass('d-none')		
				$("#product_type_title").html(title)
			});
			// for Update  REs
		</script>
