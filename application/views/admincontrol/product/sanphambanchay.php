<div class="row">
    <div class="col-lg-12 col-md-12">
        <?php if($this->session->flashdata('success')){?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('success'); ?> </div>
        <?php } ?>
        
        <?php if($this->session->flashdata('error')){?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('error'); ?> </div>
        <?php } ?>
    </div>
</div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Sản phẩm bán chạy</h4>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="top" onchange="getOrdersRows()">
                                        <option value="25" selected>TOP 25</option>
                                        <option value="50" >TOP 50</option>
                                        <option value="75" >TOP 75</option>
                                        <option value="100" >TOP 100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-rep-plugin">
                            <div class="table-responsive b-0" data-pattern="priority-columns">

                                <section class="empty-div d-none">
                                    <div class="text-center">
                                    <img class="img-responsive" src="<?php echo base_url(); ?>assets/vertical/assets/images/no-data-2.png" style="margin-top:100px;">
                                     <h3 class="m-t-40 text-center text-muted"><?= __('admin.no_orders') ?></h3></div>
                                </section>

                                <table id="orders-table" class="table table-striped">
                                    <thead class="blue-ng-order">
                                        <tr>
                                            <th  class="txt-cntr">#</th>
                                            <th  class="txt-cntr">Mã</th>
                                            <th  class="txt-cntr">Tên sản phẩm</th>
                                            <th  class="txt-cntr">SKU</th>
                                            <th  class="txt-cntr">Số lượng đã bán</th>
                                            <th  class="txt-cntr">Tồn kho</th>
                                                                                                                 
                                            
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="100%" class="text-center">
                                                <h3 class="text-muted py-4"><?= __("admin.loading_orders_data_text") ?> </h3>
                                                <h5 class="text-muted py-4"><?= __("admin.not_taking_longer") ?> </h5>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>

        <div class="modal" id="model-confirmodal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title m-0"><?= __('admin.change_order_status') ?></h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="complete-text">
                            <p class="text-center"><?= __('admin.change_order_status_information_1') ?></p> 
                            <p class="text-center"><?= __('admin.change_order_status_information_2') ?></p>
                            <br>
                        </div>
                        <p class="text-center"><b><?= __('admin.are_you_sure') ?></b></p>
                        <div class="text-center modal-buttons">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('admin.close') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
    


    function getOrdersRows(){
        $this = $(this);

        limit = $('#top').val();

        $.ajax({
            url:"<?= base_url('admincontrol/sanphambanchay'); ?>",
            type:'POST',
            dataType:'json',
            data:{getOrdersRows:1, limit :limit},
            beforeSend:function(){$this.btn("loading");},
            complete:function(){$this.btn("reset");},
            success:function(json){
                if(json['view']){
                    $("#orders-table tbody").html(json['view']);
                    $("#orders-table").show();
                } else {
                    $(".empty-div").removeClass("d-none");
                    $("#orders-table").hide();
                }

                $("#orders-table .pagination-td").html(json['pagination']);
            },
        })
    }

    $(function() {
        getOrdersRows();
    });


</script>