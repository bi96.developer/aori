<?php 
	$db =& get_instance();
	$userdetails=$db->userdetails();
	$pro_setting = $this->Product_model->getSettings('productsetting');
	$vendor_setting = $this->Product_model->getSettings('vendor');
?>

<?php foreach($productlist as $product){ ?>
	<?php 
		$productLink = base_url('store/'. base64_encode($userdetails['id']) .'/product/'.$product['product_slug'] );
	?>
	<tr>
		<td class="text-center">
			<input name="product[]" class="list-checkbox" type="checkbox" id="check<?php echo $product['product_id'];?>" value="<?php echo $product['product_id'];?>" onclick="checkonly(this,'check<?php echo $product['product_id'];?>')">
			<?php if($product['product_type'] == 'downloadable'){ ?>
				<img src="<?= base_url('assets/images/download.png') ?>" width="20px" class='d-block'>
			<?php } ?>
		</td>
		<td>
			<div class="tooltip-copy">
				<img width="50px" height="50px" src="<?php echo resize('assets/images/product/upload/thumb/'. $product['product_featured_image'] ,100,100) ?>" ><br>
			</div>
		</td>
		<td class="white-space-normal">
			<div class="tooltip-copy">
				<span><?php echo $product['product_name'];?></span>
				<div> <small>
					<a target="_blank" href="<?= $productLink.'?preview=1' ?>"><?= __('admin.public_page') ?></a>
				</small></div>
			</div>
		</td>
		<td class="txt-cntr"><?php echo $product['seller_username'] ? $product['seller_username'] : __('admin.admin'); ?></td>
		<td class="txt-cntr"><?php echo c_format($product['product_price']); ?></td>
		<td class="txt-cntr"><?php echo $product['product_sku'];?></td>
		<td class="txt-cntr"><?php echo $product['ton_kho'];?></td>
		<td class="txt-cntr">
			<?= product_status_on_store_admin($product['on_store'], $product['product_created_by']) ?>	
			<?= product_status($product['product_status']) ?>	
		</td>
		<td>
			<!-- <a class="btn btn-primary" onclick="return confirm('<?= __('admin.are_you_sure_to_edit') ?>');" href="<?php echo base_url();?>admincontrol/updateproduct/<?php echo $product['product_id'];?>"><i class="fa fa-edit cursors" aria-hidden="true"></i></a> -->
			<a class="btn btn-success" href="<?php echo base_url();?>admincontrol/nhapkho/<?php echo $product['product_id'];?>"><i class="fa fa-plus cursors" aria-hidden="true"></i></a>
			
		</td>
	</tr>
<?php } ?>