<?php foreach($orders as $index => $order){ ?>
	<?php if($order['type'] == 'store'){ ?>
		<tr>
			<td><?= orderId($order['id']);?></td>
			<td class="txt-cntr"><?php echo c_format($order['total']); ?></td>
			<td class="txt-cntr"><?php echo $order['username'];?></td>
			<td class="txt-cntr"><?php echo $order['cusname'];?></td>	
			<td class="txt-cntr"><?php echo $order['cusaddress'];?></td>		
			<?php 
				$icon = strtolower(str_replace(" ", "_", $status[$order['status']])) .'.png';
			?>
			<td class="txt-cntr">
				<div class="badge <?= ($order['status'] == 1) ? 'badge-success' : 'badge-warning' ?>">
					<?= $status[$order['status']] ?>
				</div>
			</td>
			
			<td class="txt-cntr"><?= date("d-m-Y h:i A",strtotime($order['created_at'])); ?></td>
			
		</tr>
		<?php } ?>
<?php } ?>

<script type="text/javascript">
	var jsOrders = <?= json_encode($jsOrders) ?>;
</script>