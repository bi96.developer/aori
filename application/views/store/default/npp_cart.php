<?php if($products) { ?>
<form method="POST" id="cart-form">
    <div class="card-body">
            <div class="table-responsive">
                <table class="table orders-table">
                    <thead>
                        <tr>
                            <th width="80px">#</th>
                            <th width="180px">Ảnh</th>
                            
                            <th width="280px">Tên sản phẩm</th>                        
                            <th>Số lượng</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>                      
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $key => $product) { ?>
                            <tr>
                                <td><button type="button" class="btn btn-xs btn-remove-cart text-danger" data-href="<?= $base_url."cart/?checkout_page=true&remove=".$product['cart_id'] ?>"><i class="far fa-times-circle"></i></button><?=$key+1 ?></td>
                                <td>
                                    <a class="thumbnail pull-left" href="<?= $product['link'] ?>"> 
                                        <img class="media-object" src="<?= $product['product_featured_image'] ?>" width='100%'> 
                                    </a>
                                </td>
                                <td>
                                    <?= $product['product_name'] ?>
                                </td>
                                 <td>
                                    <input class="qty-input" name="quantity[<?= $product['cart_id'] ?>]" type="text" value="<?= $product['quantity'] ?>" min="1">
                                </td>
                                 <td>
                                     <?= c_format($product['product_price']) ?>
                                </td>
                                 <td>
                                    <?= c_format($product['total']) ?>
                                </td>
                            </tr>
                        <?php } ?>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5" style="text-align:right;"><b>Tổng cộng</b></td>                        
                             <td>
                               <b><?= c_format($total) ?></b>
                            </td>
                        </tr>                    
                    </tfoot>
            </table>
        </div>
    </div>
</form>
<?php } else { ?>
<div class="card-body">
    <div class="table-responsive">
        <span>Chưa chọn sản phẩm nào</span>
    </div>
</div>
<?php } ?>

<script type="text/javascript">
            var xhr ;

            $("#cart-form").delegate(".qty-input","change",function(){
                if(xhr && xhr.readyState != 4) xhr.abort();
                $this = $(this);
                xhr = $.ajax({
                    url:'/store/cart',
                    type:'POST',
                    dataType:'html',
                    data:$("#cart-form").serialize(),
                    beforeSend:function(){},
                    complete:function(){},
                    success:function(response){
                        //$('.cart-table').html(response);
                        updateCart();
                    }
                })
                return false;
            })

            $(document).delegate(".cart-counter button","click",function(){
                var val = $(this).parent().find("input").val();
                if($(this).hasClass("add")) { val ++ }
                    else { val -- }
                        if(val <= 0) val = 1;
                    $(this).parent().find("input").val(val).trigger("change");
                });
            
