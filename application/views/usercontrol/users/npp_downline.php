<script src="<?= base_url('assets/plugins/tree') ?>/jquery-ui-1.10.4.custom.min.js"></script>
<link href="<?= base_url('assets/plugins/tree') ?>/tabelizer.min.css?v=<?= av() ?>" media="all" rel="stylesheet" type="text/css" />
<div class="row">
	<div class="col-sm-12">
		<div class="card card-tree">
			<div class="card-header">
				<h4>Doanh số tuyến dưới của <?= $user->firstname ?> <?= $user->lastname ?></h4>	
				<div class="action">
					<button class="btn btn-primary btn-sm btn-tree-action" onclick='$("#tree").fancytree("getTree").expandAll();'><?= __('admin.open_all') ?></button>
					<button class="btn btn-primary btn-sm btn-tree-action" onclick='$("#tree").fancytree("getTree").expandAll(false);'><?= __('admin.close_all') ?></button>
				</div>
			</div>
			<div class="card-body">
				<?php if(false){ ?>
					 <div class="table-rep-plugin">
                        <div class="table-responsive b-0" data-pattern="priority-columns">
                            <table id="table1" class="table  table-striped">
								<tr data-level="header" class="header">
									<th></th>
									<th>Tên NPP</th>									
									<th><?= __('admin.email') ?></th>
									<th>Đơn hàng đã xử lý</th>
									<th>Doanh số đã xử lý</th>
									<th>Đơn hàng đang xử lý</th>
									<th>Doanh số đang xử lý</th>
									<th>Tổng đơn hàng</th>
									<th>Tổng doanh số</th>
									
								</tr>
								
								<?php foreach ($mylevel as $key => $value) { ?>
									<tr data-level="1" id="level_<?= $key+1 ?>_1">
										<td></td>
										<td class="data"><?= $value['firstname'] ?></td>
										<td class="data"><?= $value['lastname'] ?></td>
										<td class="data"><?= $value['email'] ?></td>
										<td class="data">
											<?php echo (int)$value['don_hoanthanh'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value['tien_hoanthanh']) ?>
											
										</td>
										<td class="data">
											<?php echo (int)$value['don_chuahoanthanh'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value['tien_chuahoanthanh']) ?>
											
										</td>
										<td class="data">
											<?php echo (int)$value['tatcadon'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value['tongtien']) ?>
											
										</td>
									</tr>
									<?php if(isset($value['children'])){
											foreach ($value['children'] as $key1 => $value1) { ?>
											<tr data-level="2" id="level_<?= $key + 1 ?>_<?= $key1+2 ?>">
												<td></td>
										<td class="data"><?= $value1['firstname'] ?></td>
										<td class="data"><?= $value1['lastname'] ?></td>
										<td class="data"><?= $value1['email'] ?></td>
										<td class="data">
											<?php echo (int)$value1['don_hoanthanh'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value1['tien_hoanthanh']) ?>
											
										</td>
										<td class="data">
											<?php echo (int)$value1['don_chuahoanthanh'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value1['tien_chuahoanthanh']) ?>
											
										</td>
										<td class="data">
											<?php echo (int)$value1['tatcadon'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value1['tongtien']) ?>
											
										</td>
											</tr>
											<?php if(isset($value1['children'])){
													foreach ($value1['children'] as $key2 => $value3) { ?>
													<tr data-level="3" id="level_<?= $key + 1 ?>_<?= $key1+2 ?>_<?= $key2+3 ?>">
														<td></td>
										<td class="data"><?= $value3['firstname'] ?></td>
										<td class="data"><?= $value3['lastname'] ?></td>
										<td class="data"><?= $value3['email'] ?></td>
										<td class="data">
											<?php echo (int)$value3['don_hoanthanh'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value3['tien_hoanthanh']) ?>
											
										</td>
										<td class="data">
											<?php echo (int)$value3['don_chuahoanthanh'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value3['tien_chuahoanthanh']) ?>
											
										</td>
										<td class="data">
											<?php echo (int)$value3['tatcadon'] ?> 
											
										</td>
										
										<td class="data"><?php echo c_format($value3['tongtien']) ?>
											
										</td>
													</tr>
											<?php } } ?>
											<?php } ?>
									<?php } ?>
								<?php } ?>
							</table>
						</div>
					</div>
				<?php } ?>


				<script type="text/javascript" src="<?= base_url('assets/plugins/ui/jquery-ui.min.js') ?>"></script>
				<link href="<?= base_url('assets/plugins/fancytree/skin-win8/ui.fancytree.css') ?>" rel="stylesheet" />
			    <script src="<?= base_url('assets/plugins/fancytree/jquery.fancytree.js') ?>"></script>
			    <script src="<?= base_url('assets/plugins/fancytree/jquery.fancytree.table.js') ?>"></script>
			    

			    <script type="text/javascript">
			    	$(function() {
			    		$("#tree").fancytree({
			    			checkbox: false,
			    			debugLevel: 0,
			    			checkboxAutoHide: true,
			    			titlesTabbable: true,
			    			source: { url: "<?= base_url('usercontrol/myreferal_order_ajax/'. $user->id) ?>" },
			    			extensions: ["table"],
			    			table: {
			    				indentation: 10,
			    				nodeColumnIdx: 0,
			    				checkboxColumnIdx: 0,
			    			},
			    			renderColumns: function(event, data) {
			    				var node = data.node,
			    				$tdList = $(node.tr).find(">td");

			    				var col1 = node.data.email;
			    				var col2 = node.data.don_hoanthanh;
			    				var col3 = node.data.tien_hoanthanh;
			    				var col4 = node.data.don_chuahoanthanh;
			    				var col5 = node.data.tien_chuahoanthanh;
			    				var col6 = node.data.tatcadon;
			    				var col7 = node.data.tongtien;

			    				$tdList.eq(1).html(col1);
			    				$tdList.eq(2).html(col2);
			    				$tdList.eq(3).html(col3);
			    				$tdList.eq(4).html(col4);
			    				$tdList.eq(5).html(col5);
			    				$tdList.eq(6).html(col6);
			    				$tdList.eq(7).html(col7);
			    			},
			    			modifyChild: function(event, data) {
			    				data.tree.info(event.type, data);
			    			},
			    		})
			    		$("#tree").fancytree("getTree").expandAll();
			    	});
			    </script>
			    <div class="table-responsive">
				    <table id="tree" class="table table-sm">
				    	<colgroup>
				    		<col width="250px" />
				    		<col width="120px" />
				    		<col width="120px" />
				    		<col width="120px" />
				    		<col width="120px" />
				    		<col width="120px" />
				    		<col width="120px" />
				    		<col width="120px" />
				    	</colgroup>
				    	<thead>
				    		<tr>				    			
								<th>Tên NPP</th>								
								<th><?= __('admin.email') ?></th>
								<th>Đơn đã xử lý</th>
								<th>Doanh số đã xử lý</th>
								<th>Đơn đang xử lý</th>
								<th>Doanh số đang xử lý</th>
								<th>Tổng đơn hàng</th>
								<th>Tổng doanh số</th>
				    		</tr>
				    	</thead>
				    	<tbody>
				    		<tr>
				    			<td></td>
				    			<td></td>
				    			<td></td>
				    			<td></td>
				    			<td></td>
				    			<td></td>
				    			<td></td>
				    			<td></td>
				    		</tr>
				    	</tbody>
				    </table>
			    </div>
			</div>
		</div>
	</div>
</div>