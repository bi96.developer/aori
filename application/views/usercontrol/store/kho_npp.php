<?php
   $db =& get_instance();
   $userdetails=$db->userdetails();
   $store_setting =$db->Product_model->getSettings('store');
   $Product_model =$db->Product_model;
   ?>
<div id="overlay"></div>
<div class="popupbox" style="display: none;">
   <div class="backdrop box">
      <div class="modalpopup" style="display:block;">
         <a href="javascript:void(0)" class="close js-menu-close" onclick="closePopup();"><i class="fa fa-times"></i></a>
         <div class="modalpopup-dialog">
            <div class="modalpopup-content">
               <div class="modalpopup-body">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-12">
      <?php if ($currentTheme=="sales" || $StoreStatus=="0"){ ?>
      <div class="alert alert-danger"><?= __('admin.cart_product_notice')?></div>
      <?php } ?>
      <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-success alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <?php echo $this->session->flashdata('success'); ?> 
      </div>
      <?php } ?>
      <?php if($this->session->flashdata('error')){?>
      <div class="alert alert-danger alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <?php echo $this->session->flashdata('error'); ?> 
      </div>
      <?php } ?>
   </div>
</div>
<div class="row product-page">
   <div class="col-12">
      <div class="card">
         <div class="card-header bg-blue-payment">
            <div class="card-title-white pull-left m-0">QUẢN LÝ KHO</div>
            <div class="pull-right">
                <a href="<?= base_url('admincontrol/lsnhapkho/') ?>" class="btn btn-primary btn-sm">Lịch sử nhập kho</a>
            </div>                  
         </div>
         <div class="card-body">   
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="product_tab">
                  <div class="filter">
                     <form id="filter-form">
                        <div class="row mt-5">
                           <div class="form-group col-4">
                              <select name="category_id" class="form-control select-category">
                                 <?php $selected = isset($_GET['category_id']) ? $_GET['category_id'] : ''; ?>
                                 <option value=""><?= __('admin.all_category') ?></option>
                                 <?php foreach ($categories as $key => $value) { ?>
                                 <option <?= $selected == $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="form-group col-4">
                              <select name="seller_id" class="form-control select-vendor">
                                 <?php $selected = isset($_GET['seller_id']) ? $_GET['seller_id'] : ''; ?>
                                 <option value=""><?= __('admin.all_vendor') ?></option>
                                 <?php foreach ($vendors as $key => $value) { ?>
                                 <option <?= $selected == $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                 <?php } ?>
                              </select>
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="table-rep-plugin">
                     <?php if ($productlist == null) {?>
                     <div class="text-center">
                        <img class="img-responsive" src="<?php echo base_url(); ?>assets/vertical/assets/images/no-data-2.png" style="margin-top:100px;">
                        <h3 class="m-t-40 text-center text-muted"><?= __('admin.no_products') ?></h3>
                     </div>
                     <?php } else { ?>

                        <form method="post" name="deleteAllproducts" id="deleteAllproducts" action="<?php echo base_url('admincontrol/deleteAllproducts'); ?>">
                           <div class="table-responsive">
                           <table id="tech-companies-1" class="table table-striped btn-part">
                              <thead>
                                 <tr>
                                    <th><input name="product[]" type="checkbox" value="" onclick="checkAll(this)"></th>
                                    <th><?= __('admin.image') ?></th>
                                    <th><?= __('admin.product_name') ?></th>
                                    <th><?= __('admin.user') ?></th>
                                    <th><?= __('admin.price') ?></th>
                                    <th><?= __('admin.sku') ?></th>
                                    <th>Tồn kho</th>
                                    <th><?= __('admin.status') ?></th>
                                    <th><?= __('admin.action') ?></th>
                                 </tr>
                              </thead>
                              <tbody></tbody>
                              <tfoot>
                                 <tr>
                                    <td colspan="12" class="text-right">
                                       <ul class="pagination pagination-td"></ul>
                                    </td>
                                 </tr>
                              </tfoot>
                           </table>
                     </div>
                        </form>
                     <?php } ?>
                  </div>
               </div>
              
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?= $social_share_modal ?>
<script type="text/javascript" async="">
    $('.forms-nnnav li a').on('shown.bs.tab', function(event){
        var x = $(event.target).attr('href');
        $(".btn-submit").hide();
   
        if(x != '#site-fronttemplate'){
            $(".btn-submit").show();
        }
        localStorage.setItem("last_pill", x);
    });

    $('.product_tab_option').on('click', function(){
        $(".product-options").show();
    });

    $('.product_coupons_tab_option').on('click', function(){
        $(".product-options").hide();
    });

    $(document).on('ready',function() {
        var last_pill = localStorage.getItem("last_pill");
        if (last_pill == "#product_coupons_tab") {
            $(".product-options").hide();
        }else{
            $(".product-options").show();
        }
        if(last_pill){ $('[href="'+ last_pill +'"]').click() }
    });
   
    $temp_import_product_data = null;
   
    $('#bulk_products_form_btn').on('click', function(e){ 
        e.preventDefault();
        $("#bulk_products_form .alert-danger").remove();
        if($('#bulk_products_form input[name="file"]').val()) {
            $this = $(this);
            var fd = new FormData(document.getElementById("bulk_products_form"));
   
            $.ajax({
                url: '<?= base_url('admincontrol/bulkProductImport'); ?>',  
                type: 'POST',
                data: fd,
                dataType: 'html',
                beforeSend:function(){$this.btn("loading");},
                complete:function(){
                    $this.btn("reset");
                    $('#manageBulkProducts').modal('hide');
                },
                success:function(response){               
                    $('#manageBulkProductsConfirmation .modal-body').html(response);
                    $('#manageBulkProductsConfirmation').modal('show');
   
                    if(! $('#manageBulkProductsConfirmation textarea[name="product_for_import"]').length > 0) {
                        $('#manageBulkProductsConfirmation .import-products-confirm').hide();  
                    } else {
                        $('#manageBulkProductsConfirmation .import-products-confirm').show();  
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });   
        } else {
           $("#bulk_products_form .custom-file").after('<div class="alert alert-danger"><?= __('admin.please_select_excel_file') ?></div>');
        }
    });
   
    $('#manageBulkProductsConfirmation .import-products-confirm').on('click', function(e){
        e.preventDefault();
        if($('#manageBulkProductsConfirmation textarea[name="product_for_import"]').val()) {
            $this = $(this);
            var data = new FormData();
            data.append( 'products', $('#manageBulkProductsConfirmation textarea[name="product_for_import"]').val());
            $.ajax({
                url: '<?= base_url('admincontrol/bulkProductImportConfirm'); ?>',  
                type: 'POST',
                data: data,
                beforeSend:function(){$this.btn("loading");},
                complete:function(){
                    $this.btn("reset");
                    $('#manageBulkProductsConfirmation').modal('hide');
                },
                success:function(response){               
                    $('#manageBulkProductsResult .modal-body').html(response);
                    $('#manageBulkProductsResult').modal('show');
                },
                cache: false,
                contentType: false,
                processData: false
            });   
        } else {
            $("#bulk_products_form .custom-file").after('<div class="alert alert-danger"><?= __('admin.please_select_excel_file') ?></div>');
        }
    });
   
    $(".show-more").on('click',function(){
        $(this).parents("tfoot").remove();
        $("#product-list tr.d-none").hide().removeClass('d-none').fadeIn();
    });
   
    $(".delete-button").on('click',function(){
        if(!confirm("<?= __('admin.are_you_sure') ?>")) return false;
    });

    $(document).on('ready',function(){
      $('.delete-form-button').on('click',function(){
         var r = confirm("<?= __("admin.delete_form_confirmation") ?>");
         if (r == true) {         
            location = $(this).data("href");
         }
         return false;
      })
    })

    $(".toggle-child-tr").on('click',function(){
        $tr = $(this).parents("tr");
        $ntr = $tr.next("tr.detail-tr");
       
        if($ntr.css("display") == 'table-row'){
            $ntr.hide();
            $(this).find("i").attr("class","fa fa-plus");
        }else{
            $(this).find("i").attr("class","fa fa-minus");
            $ntr.show();
        }
    })
   
    function checkAll(bx) {
        var cbs = document.getElementsByTagName('input');
            if(bx.checked)
        {
           // document.getElementById('deletebutton').style.display = 'block';
        } else {
            //document.getElementById('deletebutton').style.display = 'none';
        }
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = bx.checked;
            }
        }
    }
    
    function checkAllForm(bx) {
      var cbs = document.getElementsByTagName('input');
      if(bx.checked)
      {
         //document.getElementById('deletebuttonform').style.display = 'block';
         } else {
         //document.getElementById('deletebuttonform').style.display = 'none';
      }
      for(var i=0; i < cbs.length; i++) {
         if(cbs[i].type == 'checkbox') {
            cbs[i].checked = bx.checked;
         }
      }
    }

    function checkonly(bx,checkid) {
        if($(".list-checkbox:checked").length){
            $('#deletebutton').show();
        } else {
            $('#deletebutton').hide();
        }
    }
   
    function deleteuserlistfunc(formId){
        if(! confirm("<?= __('admin.are_you_sure') ?>")) return false;
   
        $('#'+formId).submit();
    }
   
    function deleteformfunc(formId){
      if(! confirm("<?= __('admin.are_you_sure') ?>")) return false;

      $('#'+formId).submit();
    }

    $("#filter-form").on("submit",function(){
        getPage('<?= base_url("admincontrol/listproduct_ajax/") ?>/1');
        return false;
    })

    $(".select-category, .select-vendor").on("change",function(){
        $("#filter-form").submit();
    })
   
    function getPage(url){
       var category_id = $('.select-category').find(":selected").val();
       var seller_id = $('.select-vendor').find(":selected").val();
       $this = $(this);
       $.ajax({
            url:url,
            type:'POST',
            dataType:'json',
            data:$("#filter-form").serialize(),
            beforeSend:function(){$this.btn("loading");},
            complete:function(){$this.btn("reset");},
            success:function(json){
               if(json['view']){
                  $("#tech-companies-1 tbody").html(json['view']);
                  $("#tech-companies-1").show();
               } else {
                  $(".empty-div").removeClass("d-none");
                  $("#tech-companies-1").hide();
               }
        
               $("#tech-companies-1 .pagination-td").html(json['pagination']);
            },
       });
    }
   
    $(document).on('click', '.export-products-btn', function() {
        exportProducts($(this), 0);
    });
   
    $(document).on('click', '.export-structure-btn', function() {
        exportProducts($(this), 1);
    });
   
    function exportProducts(thatBtn, structure_only  = 0) {
        $.ajax({
            url:'<?= base_url("admincontrol/exportproduct/") ?>',
            type:'POST',
            dataType:'json',
            data:{structure_only:structure_only},
                beforeSend:function(){thatBtn.btn("loading");},
                complete:function(){thatBtn.btn("reset");},
                success:function(json){
            if(json['download']){
                window.location.href = json['download'];
            }
        },
        });
    }
   
   
    getPage('<?= base_url("admincontrol/warehouse_ajax/") ?>/1');
        $("#tech-companies-1 .pagination-td").delegate("a","click",function(){
        getPage($(this).attr("href"));
        return false;
    })
   
    function closePopup(){
       $('.popupbox').hide();
       $('#overlay').hide();
    }
   function generateCode(affiliate_id){
        $('.popupbox').show();
        $('#overlay').show();
        $('.modalpopup-body').load('<?php echo base_url();?>admincontrol/generateproductcode/'+affiliate_id);
        $('.popupbox').ready(function () {
            $('.backdrop, .box').animate({
            'opacity': '.50'
            }, 300, 'linear');
            $('.box').animate({
                'opacity': '1.00'
            }, 300, 'linear');
            $('.backdrop, .box').css('display', 'block');
        });
   }
   
   $(document).delegate(".delete-product",'click',function(){
       if(! confirm("<?= __('admin.are_you_sure') ?>")) return false;
       window.location = $("#deleteAllproducts").attr("action") + "?delete_id=" + $(this).attr("data-id");
   })
</script>