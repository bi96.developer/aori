<div class="clearfix"></div>
<br>

<div class="card">
	<div class="card-header">
		<h4 class="card-title">Danh sách đơn hàng NPP tuyến dưới</h4>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label class="control-label"><?= __('user.status') ?></label>
					<select class="form-control filter_status">
						<option value=""><?= __('user.all'); ?></option>
						<?php foreach ($status as $key => $value) { ?>
							<option value="<?= $key ?>">
								<?php   
									if ($value == 'Received') {
										echo __('user.received');
									}elseif ($value == 'Complete') {
										echo __('user.complete');
									}elseif ($value == 'Total not match') {
										echo __('user.total_not_match');
									}elseif ($value == 'Denied') {
										echo __('user.denied');
									}elseif ($value == 'Expired') {
										echo __('user.expired');
									}elseif ($value == 'Failed') {
										echo __('user.failed');
									}elseif ($value == 'Processed') {
										echo __('user.processed');
									}elseif ($value == 'Refunded') {
										echo __('user.refunded');
									}elseif ($value == 'Reversed') {
										echo __('user.reversed');
									}elseif ($value == 'Voided') {
										echo __('user.voided');
									}elseif ($value == 'Canceled Reversal') {
										echo __('user.cancel_reversal');
									}elseif ($value == 'Waiting For Payment') {
										echo __('user.waiting_for_payment');
									}elseif ($value == 'Pending') {
										echo __('user.pending');
									}else{
										echo $value;
									}
								?>
							</option>
						<?php } ?>
					</select>
					
				</div>
			</div>

			<div class="col-sm-2">
				<div class="form-group">
					<label class="control-label">Ví mua hàng</label>
					
					</select>
					<select class="form-control filter_wallet">
						<option value="">Tất cả</option>
						<option value="1">Ví 1</option>
						<option value="2">Ví 2</option>
						
							
						
					</select>
				</div>
			</div>
			
			<div class="col-sm-2">
				<div class="form-group">
					<label class="control-label d-block">&nbsp;</label>
					<button class="btn btn-primary" onclick="getPage(1,this)"><?= __('user.search') ?></button>
                	
            
					
				</div>
			</div>
			
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table orders-table">
				<thead>
					<tr>
						<th width="80px">#</th>
						<th width="80px"><?= __('user.order_id') ?></th>
						<th>NPP</th>
						<th><?= __('user.total') ?></th>
						
						<th><?= __('user.store') ?></th>
						<th><?= __('user.status') ?></th>
						<th>Ví trừ tiền</th>
						
						<th><?= __('user.date') ?></th>
                        
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	<div class="card-footer text-right" style="display: none;"> <div class="pagination"></div> </div>
</div>
<div id="orderModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tạo đơn hàng</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <form id="order-create-form" action="/usercontrol/confirm_sanpham_order" method="post" data-products="" data-paymentmethod="<?php echo __('user.cash_on_delivery') ?>">
                    <input type="hidden" value="0" name="order_id" />
                    <input type="hidden" value="0" name="user_id" />

                    <div class="form-group">
                        <label>Họ tên</label>
                        <div class="input-group">
                            <input type="text" class="form-control p-4" name="hoten_donhang" placeholder="Nhập họ và tên...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Số điện thoại</label>
                        <div class="input-group">
                            <input type="text" class="form-control p-4" name="sdt_donhang" placeholder="Nhập số điện thoại...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <div class="input-group">
                            <input type="text" class="form-control p-4" name="diachi_donhang" placeholder="Nhập địa chỉ...">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-5">
                                <label>Chọn sản phẩm</label>
                                <select id="all_categories" class="form-control" >

                                </select>
                            </div>
                            <div class="col-sm-5">
                                <label>Theo tên sản phẩm</label>
                                <input name="search" id="key_word_product" type="text" value="" placeholder="Theo tên sản phẩm" class="form-control" />
                            </div>
                            <div class="col-sm-2">
                                <label>&nbsp;</label>
                                <button class="btn btn-info btn-block" onclick="loadProducts()" type="button"><i class="fa fa-filter"></i> Lọc</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Chọn sản phẩm</label>
                        <div class="product-slide">
                        </div>
                        <div class="well product-choose-well" style="margin-top:15px;display: none;">
                            <h6>Sản phẩm đã chọn</h6>
                            <div class="product-choose">
                                <div class="head-row">
                                    <div class="row">
                                        <div class="col-sm-5 col-5"> <div><label>Sản phẩm</label></div></div>
                                        <div class="col-sm-2 col-2"> <div><label>Giá</label></div></div>
                                        <div class="col-sm-2 col-3"> <div><label>S.Lượng</label></div></div>
                                        <div class="col-sm-2 d-none d-sm-block"> <div><label>Tổng</label></div></div>
                                        <div class="col-sm-1 col-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Tạo đơn hàng</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
	$(document).on('click', '.btn-order-create', function () {

        $('#orderModal').modal('show');
        $('#orderModal').find('.modal-title').text('Tạo đơn hàng');
        $('#orderModal').find('button[type="submit"]').text('Tạo đơn hàng');
        $('#orderModal').find('form input[name="order_id"]').val("0");
        $('#orderModal').find('form input[name="user_id"]').val("0");
        $('#orderModal').find('form').data("products", '');
        $('#orderModal').find('form').data("paymentmethod", '<?php echo __('user.cash_on_delivery') ?>');

        $.ajax({
            url: '/usercontrol/get_danhmuc_sanpham/',
            type: 'POST',
            success: function (result) {
                $('#all_categories').html(result);
                loadProducts();
            }
        });


    });
        $(document).on('click', '.btn-shipping-create', function () {

        $('#orderModal').modal('show');

        $.ajax({
            url: '/usercontrol/get_sanpham_order/',
            type: 'POST',
            success: function (result) {
                $('#all_sanpham').html(result);
            }
        });

    });

    $("#orderModal form").on('submit', function () {
        $this = $(this);
        $.ajax({
            url: '<?= base_url('usercontrol/save_sanpham_order/') ?>',
            type: 'POST',
            dataType: 'json',
            data: $this.serialize(),
            beforeSend: function () {
                $this.find(".btn-submit").button("loading");
            },
            complete: function () {
                $this.find(".btn-submit").button("reset");
            },
            success: function (result) {
                $this.find(".has-error").removeClass("has-error");
                $this.find("span.text-danger").remove();

                if (result['location']) {
                    window.location = result['location'];
                }
                if (result['errors']) {

                    $.each(result['errors'], function (i, j) {
                        if (i === 'all_sanpham') {
                            $ele = $this.find('.product-slide');
                        } else {
                            $ele = $this.find('[name="' + i + '"]');
                        }

                        if ($ele) {
                            $ele.parents(".form-group").addClass("has-error");
                            $ele.after("<span class='text-danger error-elem'>" + j + "</span>");
                        }
                    });
                } else {
                    removeError();
                }
            }
        });
        return false;
    });
    function removeError() {
        $("#orderModal form").find(".form-group.has-error").removeClass('has-error');
        $("#orderModal form").find(".error-elem").remove();
    }
    $(document).on('click', '.edit-order', function () {
        $this = $(this);

        $.ajax({
            url: '/usercontrol/get_order/',
            type: 'POST',
            dataType: 'json',
            data: {id: $this.attr("data-id")},
            beforeSend: function () {
                $this.btn("loading");
            },
            complete: function () {
                $this.btn("reset");
            },
            success: function (json) {
                console.log(json);
                if (json && json.order) {
                    $('#orderModal').modal('show');
                    $('#orderModal').find('.modal-title').text('Sửa đơn hàng');
                    $('#orderModal').find('button[type="submit"]').text('Sửa đơn hàng');
                    $('#orderModal').find('input[name="hoten_donhang"]').val(json.order.firstname);
                    $('#orderModal').find('input[name="sdt_donhang"]').val(json.order.phone);
                    $('#orderModal').find('input[name="diachi_donhang"]').val(json.order.address);
                    $('#orderModal').find('input[name="user_id"]').val(json.order.user_id);
                    $('#orderModal').find('input[name="order_id"]').val(json.order.id);
                    $('#orderModal').find('form').data('paymentmethod', json.order.payment_method);
                    setTimeout(loadProducts(json.productIds), 300);
                   
                }

            }
        });
    });
	 $(".orders-table").delegate(".toggle-child-tr","click",function(){
        $tr = $(this).parents("tr");
        $ntr = $tr.next("tr.detail-tr");

        if($ntr.css("display") == 'table-row'){
            $ntr.hide();
            $(this).find("i").attr("class","fa fa-plus");
        }else{
            $(this).find("i").attr("class","fa fa-minus");
            $ntr.show();
        }
    })
    
	function getPage(page,t) {
		$this = $(t);
		var data ={
			page:page,
			filter_status:$(".filter_status").val(),
			filter_wallet:$(".filter_wallet").val()
		}
		$.ajax({
			url:'<?= base_url("usercontrol/npp_orders_tree") ?>/' + page,
			type:'POST',
			dataType:'json',
			data:data,
			beforeSend:function(){$this.btn("loading");},
			complete:function(){$this.btn("reset");},
			success:function(json){
				$(".orders-table tbody").html(json['html']);
				$(".card-footer").hide();
				
				if(json['pagination']){
					$(".card-footer").show();
					$(".card-footer .pagination").html(json['pagination'])
				}
			},
		})
	}

	$(".card-footer .pagination").delegate("a","click", function(e){
		e.preventDefault();
		getPage($(this).attr("data-ci-pagination-page"),$(this));
	})

	getPage(1)

</script>