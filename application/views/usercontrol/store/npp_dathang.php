<link rel="stylesheet" href="<?= base_url('assets/store/default/css/style.css'); ?>" />
<div class="clearfix"></div>
<br>
<div class="card">
	<div class="card-header">
		<h4 class="card-title">Thông tin nhận hàng </h4> 
	</div>
	<div class="card-body cart-top">
		<div class="form-group">
			<label class="col-form-label">Họ và tên</label>
			<div>
				<input placeholder="Nhập họ tên người nhận" name="cusname" id="cusname" value="" class="form-control" type="text">
			</div>
		</div>
		<div class="form-group">
			<label class="col-form-label">Điện thoại</label>
			<div>
				<input placeholder="Nhập điện thoại người nhận" name="cusphone" id="cusphone" value="" class="form-control" type="text">
			</div>
		</div>
		<div class="form-group">
			<label class="col-form-label">Địa chỉ</label>
			<div>
				<input placeholder="Nhập địa chỉ người nhận" name="cusaddress" id="cusaddress" value="" class="form-control" type="text">
			</div>
		</div>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<h4 class="card-title">Sản phẩm đã chọn </h4> 
	</div>
	<div class="card-body cart-top">
		<button type="button" class="btn btn-success btn-md btn-product-add"><i class="fa fa-plus"></i> Thêm sản phẩm</button>
		<!-- <span class="cart-count position-absolute">0</span> -->
		<div class="cart-table">
            <div class="card-body">
			    <div class="table-responsive">
			        <span>Chưa chọn sản phẩm nào</span>
			    </div>
			</div>
        </div>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<button type="button" class="btn btn-primary btn-md btn-order-create">Tạo đơn hàng</button>
	</div>
</div>

<div class="modal fade" id="productModal" tabindex="-1" aria-labelledby="productModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="max-width: 950px !important;">
  <div class="modal-content">
  	<div class="modal-header">
	    <h4 class="modal-title">Danh sách sản phẩm</h4>
	    <button type="button" class="close" data-dismiss="modal">×</button>
	</div>
    <div class="modal-body">
		<div class="home-trend-top d-flex justify-content-between" style="padding:5px !important">			
			<div class="searchbox">
				<input id="searchProduct" type="text" placeholder="<?= __('store.search') ?>" />
				<img src="<?= base_url('assets/store/default/'); ?>img/search.png" class="search-icon-home" alt="<?= __('store.search') ?>">			
			</div>
			<div class="sort-filter">
					<label>Loại sản phẩm: </label>
					<select id="sort-by">
						<option value="">Tất cả</option>	
						<?php foreach ($category as $item) { ?>
								<option value="<?=$item['id'] ?>"><?php echo $item['name']?></option>						
						<?php } ?>
					</select>
				</div>

		</div> 
		
		<section class="home-product-grid">
				<div class="product-row d-flex flex-wrap product-list-trending">
					
				</div>
		</section>
		<a href="javascript:void(0);" class="see-more see-more-trendings" data-next_page="1" data-request_page_section="trending">
			<img alt="<?= __('store.image') ?>" src="<?= base_url('assets/store/default/'); ?>img/loading.png" /> <?= __('store.show_more') ?>
		</a>
	
    </div>
  </div>
  </div>
</div>
<div class="modal fade" id="cart-confirm" tabindex="-1" aria-labelledby="cart-confirm" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="popup-content">
    <img src="<?= base_url('assets/store/default/'); ?>img/shopping-cart.png" class="pop-cart-img" alt="<?= __('store.icon') ?>">
    <h2 id="product-name-prev"></h2>
    <p>Tạo đơn hàng thành công</p>
    <img src="<?= base_url('assets/store/default/'); ?>img/popline.png" class="img-fluid img-popline" alt="<?= __('store.icon') ?>">
    <div class="pop-btn-row">
      <a href="./npp_orders" type="button" class="btn btn-poup bg-main" >
        Xem danh sách đơn hàng
      </a>
      <a href="./npp_dathang" type="button" class="btn btn-poup bg-main" >
        Ở lại trang đặt hàng
      </a>
    </div>
    </div>
  </div>
  </div>
</div>
<?php include 'product-list-template.php';  ?>
<script type="text/javascript">
	$(document).ready(function() {
		load_Product($('#searchProduct').val());
		var categories = '';
		var search = '';

		$('#searchProduct').keyup(function(e) {
			e.preventDefault();
			search = $(this).val();
			load_Product(search,categories);
		});

		$('#sort-by').change(function(e) {
			e.preventDefault();
			categories = $(this).val();
			load_Product(search,categories);
		});


	});
	 $(document).on('click', '.see-more', function() {
		load_Product(null, {
			next_page: $(this).data('next_page'),
			request_page_section: $(this).data('request_page_section')
		});
	});

	function load_Product(search,categories, postData = {}) {
		var data = postData;
		data.search = search;
		data.categories = categories;
		data.request_page = 'home';
		var ajaxReq = 'ToCancelPrevReq';
		var ajaxReq = $.ajax({
			url: "<?= base_url() ?>" + 'Store/load_Product',
			type: 'POST',
			dataType: 'JSON',
			data: data,
			beforeSend : function() {
				if(ajaxReq != 'ToCancelPrevReq' && ajaxReq.readyState < 4) {
					ajaxReq.abort();
				}
				$('.btn-search').addClass('btn-loading');
			},
			complete : function() {
				$('.btn-search').removeClass('btn-loading');
			},
			success: function(res) {
				if(res.trendings) {
					if(postData.next_page && postData.next_page > 1) {
						$('.product-list-trending').append(Mustache.render($('#product-list-template').html(), res.trendings));
					} else {
						$('.product-list-trending').html(Mustache.render($('#product-list-template').html(), res.trendings));
					}
					$('.see-more-trendings').data('next_page', res.trendings.next_page);
					if(res.trendings.is_last_page) {
						$('.see-more-trendings').hide();
					}
				}

				if(res.new) 
				{
					
					if(postData.next_page && postData.next_page > 1) {
						$('.product-list-new').append(Mustache.render($('#product-list-template').html(), res.new));
					} else {
						$('.product-list-new').html(Mustache.render($('#product-list-template').html(), res.new));
					}
					$('.see-more-new').data('next_page', res.new.next_page);
					if(res.new.is_last_page) {
						$('.see-more-new').hide();
					}
				}

				if(res.category.new && res.category.new.length) {
					$('.home-new-products .category-listing').html(res.category.new);
				}

				if(res.category.all && res.category.all.length) {
					$(".demo-cat-badge").hide();
				}
			}
		});
	}
</script>
<script src="<?= base_url() ?>/assets/plugins/mustache.js"></script>


<script type="text/javascript">
 function updateCart(){
      $.ajax({
          url:'<?= base_url() ?>/store//mini_cart',
          type:'POST',
          dataType:'json',
          beforeSend:function(){},
          complete:function(){},
          success:function(json){
              $(".cart-top .cart-table").html(json['nppcart']);
              $(".cart-top .cart-count").html(json['total']);
              $('#cart-sub-total').text(json['sub_total']);
          },
      });
    }

  $(function(){ 
    $(document).on('click', ".btn-cart", function(){
      let quantity = ($('input#product-quantity').length) ? $('input#product-quantity').val() : 1;
      let product_name = $(this).data('product_name');
      let product_id = $(this).data('product_id');
      $this = $(this);    
     
	    $.ajax({
	      url:'<?= base_url() ?>/store/add_to_cart',
	      type:'POST',
	      dataType:'json',
	      data: {
	        quantity:quantity,
	        product_id:product_id,
	        //variation:variationSelected,
	      },
	      beforeSend: function(){$this.btn("loading");},
	      complete: function(){$this.btn("reset");},
	      success: function(json) {
	        if(json['location']){
	          updateCart();
	          //window.location = json['location'];
	        //  $('#cart-confirm #product-name-prev').text(product_name)
	         //$("#cart-confirm").modal("show");
	        }
	      }
	    });
     
    });
     $(document).on('click', '.btn-product-add', function () {

        $('#productModal').modal('show');

    });
     $(document).on('click', '.btn-order-create', function () {

     	cusname = $('#cusname').val();
     	cusphone = $('#cusphone').val();
     	cusaddress = $('#cusaddress').val();
     	 $this = $(this);

     	$.ajax({
	      url:'<?= base_url() ?>/usercontrol/create_order',
	      type:'POST',
	      dataType:'json',
	      data: {
	        cusname:cusname,
	        cusphone:cusphone,
	        cusaddress:cusaddress,
	        //variation:variationSelected,
	      },
	      beforeSend: function(){$this.btn("loading");},
	      complete: function(){$this.btn("reset");},
	      success: function(json) {
	      	
	        $("#cart-confirm").modal("show");

	      }
	    });

        

    });

    
    $(document).on("click", ".cart-table .btn-remove-cart", function(){
      $this = $(this);
      $.ajax({
          url:$this.attr("data-href"),
          type:'POST',
          dataType:'json',
          beforeSend:function(){},
          complete:function(){},
          success:function(json){
              updateCart();              
          },
      })
      return false;
    });

    $(document).on('click', ".cart-top", function(){
      $(".js-dropdown-list").hide();
      $(".js-dropdown-list1").hide();
      $(".js-dropdown-list2").hide();
      $(".cart-dropdown").slideToggle();
    });

    updateCart();
  });
  </script>