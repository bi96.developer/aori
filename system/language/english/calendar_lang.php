<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cal_su'] = 'CN';
$lang['cal_mo'] = 'T2';
$lang['cal_tu'] = 'T3';
$lang['cal_we'] = 'T4';
$lang['cal_th'] = 'T5';
$lang['cal_fr'] = 'T6';
$lang['cal_sa'] = 'T7';
$lang['cal_sun'] = 'CN';
$lang['cal_mon'] = 'T2';
$lang['cal_tue'] = 'T3';
$lang['cal_wed'] = 'T4';
$lang['cal_thu'] = 'T5';
$lang['cal_fri'] = 'T7';
$lang['cal_sat'] = 'T7';
$lang['cal_sunday'] = 'Chủ nhật';
$lang['cal_monday'] = 'Thứ hai';
$lang['cal_tuesday'] = 'Thứ 3';
$lang['cal_wednesday'] = 'Thứ 4';
$lang['cal_thursday'] = 'Thứ 5';
$lang['cal_friday'] = 'Thứ 6';
$lang['cal_saturday'] = 'Thứ 7';
$lang['cal_jan'] = '01';
$lang['cal_feb'] = '02';
$lang['cal_mar'] = '03';
$lang['cal_apr'] = '04';
$lang['cal_may'] = '05';
$lang['cal_jun'] = '06';
$lang['cal_jul'] = '07';
$lang['cal_aug'] = '08';
$lang['cal_sep'] = '09';
$lang['cal_oct'] = '10';
$lang['cal_nov'] = '11';
$lang['cal_dec'] = '12';
$lang['cal_january'] = 'Tháng 1';
$lang['cal_february'] = 'Tháng 2';
$lang['cal_march'] = 'Tháng 3';
$lang['cal_april'] = 'Tháng 4';
$lang['cal_mayl'] = 'Tháng 5';
$lang['cal_june'] = 'Tháng 6';
$lang['cal_july'] = 'Tháng 7';
$lang['cal_august'] = 'Tháng 8';
$lang['cal_september'] = 'Tháng 9';
$lang['cal_october'] = 'Tháng 10';
$lang['cal_november'] = 'Tháng 11';
$lang['cal_december'] = 'Tháng 12';
